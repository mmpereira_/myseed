module.exports = function(config){
  config.set({

    basePath : './',

    files : [
      'lib/angular.js',
      'lib/angular-route.js',
      'lib/angular-mocks.js',
      'test/**/*.js'

      // 'app/components/**/*.js',
      // 'app/view*/**/*.js'
    ],

    autoWatch : true,

    frameworks: ['jasmine'],

    browsers : ['Chrome'],

    plugins : [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-junit-reporter'
            ],

    junitReporter : {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

  });
};
